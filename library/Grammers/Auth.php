<?php

class Grammers_Auth extends Zend_Auth {

    private static $__instance = null;
    private $_table = 'users';

    public static function getInstance() {
        if (null === self::$__instance) {
            self::$__instance = new self();
        }
        return (self::$__instance);
    }

    public function __construct() {
        parent::__construct();
    }

    public function isLoggued() {
        return ($this->getIdentity() !== null);
    }

    public function login($email = null, $password = null) {
        
        if(!empty($email) && !empty($password)){
            $dbAdapter = Zend_Registry::get('dbAdapter');
            $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter); 
            
            $authAdapter->setTableName($this->_table)
                        ->setIdentityColumn('email')
                        ->setCredentialColumn('password');

            $authAdapter->setIdentity($email)
                        ->setCredential(md5($email) . ':' . md5($password));
            
            $auth = Zend_Auth::getInstance();
            $result = $auth->authenticate($authAdapter);
            
            if($result->isValid())
            {
                $userInfo = $authAdapter->getResultRowObject(null, 'password');
                $authStorage = $auth->getStorage();
                $authStorage->write($userInfo);
                
                return array('message' => 'valid User', 'status' => 'ok');
            }else{
                return array('message' => 'Invalid User', 'status' => 'ko');
            }
        }
    }

    public function getIdentity() {
        $identity = parent::getIdentity();
        if ($identity) {
            return $identity;
        }
        return null;
    }

    public function logout() {
        $this->clearIdentity();
    }
}
