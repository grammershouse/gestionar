<?php

abstract class Grammers_Db_AbstractManager
{
    protected static $_instance;
    protected $_storage = null;

    public function __construct(){
        $this->_getStorage();
    }
    
    // to get a clean vo for inserting on db 
    abstract public function getInitialVo();
    abstract protected function _getStorage();
    
    public function save($object){
        if($this->_getStorage() !== null){
            $res = $this->_getStorage()->save($object);
        }else{
            throw new Exception('Storage is not defined !!');
        }
    }
    
    public function delete($id){
        if($this->_getStorage() !== null){
            $res = $this->_getStorage()->delete($id);
        }else{
            throw new Exception('Storage is not defined !!');
        }
    }
    
}