<?php
require_once 'Zend/Db/Table.php';

abstract class Grammers_Db_AbstractReader extends Zend_Db_Table
{
    protected $_db;
    
    public function __construct()
    {
        parent::__construct();
        $this->_db = Zend_Registry::get('dbAdapter');
    }
    
    abstract public function getById($id);

    public function getAll(){
        $ret = $this->fetchAll()->toArray();
        return $ret;
    }
    
}