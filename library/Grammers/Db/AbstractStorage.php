<?php

/* here we can put methods to deal with cache,
 * validation of datos with a validator ....
 * and more functions needed
 */

abstract class Grammers_Db_AbstractStorage
{
    protected $_writer = null;
    protected $_reader = null;

    public function __construct()
    {
        $this->_initReaderAndWriter();
    }
    
    protected function getReader() {
        return $this->_reader;
    }
    
    protected function getWriter() {
        return $this->_writer;
    }
    
    abstract protected function _initReaderAndWriter();    
    
}