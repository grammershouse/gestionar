<?php
require_once 'Zend/Db/Table.php';

abstract class Grammers_Db_AbstractWriter
{
    protected $_db;
    
    public function __construct()
    {
        $this->_db = Zend_Registry::get('dbAdapter');
    }
    
    abstract public function insert($object);
    abstract public function update($object);
    abstract public function deleteById($object);
}