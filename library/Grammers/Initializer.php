<?php

require_once 'Zend/Controller/Plugin/Abstract.php';
require_once 'Zend/Controller/Front.php';
require_once 'Zend/Controller/Request/Abstract.php';
require_once 'Zend/Controller/Action/HelperBroker.php';
require_once "Zend/Loader.php";
require_once 'Zend/Loader/Autoloader/Resource.php';


/**
 * Initializes configuration depending on the type of environment
 * (test, development, production, etc.)
 *
 * Used to configure environment variables, databases,
 * layouts, routers, helpers etc...
 *
 */
class Grammers_Initializer extends Zend_Controller_Plugin_Abstract
{

    protected $_root;
    protected $_front;
    protected $_config;

    /**
     * Initialize environment, root path, and configuration.
     */
    public function __construct ($env = "production")
    {
        // setup autoloader
        require_once "Zend/Loader/Autoloader.php";
        $autoloader = Zend_Loader_Autoloader::getInstance();
        $autoloader->setFallbackAutoloader(true);
//        $autoloader->suppressNotFoundWarnings(true);

        // root
        $root = realpath(dirname(__FILE__) . '/../../');
        defined('ROOT') or define('ROOT', $root);
        $this->_root = $root;

        $this->initConfig($env);

        // initializations
        $this->_front = Zend_Controller_Front::getInstance();
        $this->initControllers();

        $this->initDatabase();
        $this->initHelpers();
        $this->initRoutes();

    }

    private function initConfig($env)
    {
        $configPath = dirname(__FILE__) . "/../../app/configs/environment.ini";
        $this->_config = $config = new Zend_Config_Ini($configPath, $env);

        $base = $config->application->toArray();
        defined('URL_APP') or define('URL_APP', $base['url']);

        // timezone
        if (isset($this->_config->base->timezone)){
            $timezone = $this->_config->base->timezone;
        }
        else{
            $timezone = "America/New_York";
            date_default_timezone_set($timezone);
        }
    }

    public function initControllers ()
    {
        $front = $this->_front;
        //$front->throwExceptions(false);
       // $front->setParam('useDefaultControllerAlways', true);
        $front->addControllerDirectory($this->_root.'/app/modules/public/controllers', 'public');
        $front->addControllerDirectory($this->_root.'/app/modules/admin/controllers', 'admin');
        $front->setDefaultModule('public');

        $front->registerPlugin(new Zend_Controller_Plugin_ErrorHandler (array('controller' => 'error', 'action' => 'index', 'module' => 'public')));
    }

    public function initHelpers()
    {
        //http://framework.zend.com/manual/1.10/en/zend.controller.actionhelpers.html
        $aclHelper = new Grammers_Helpers_ActionHelper();
        Zend_Controller_Action_HelperBroker::addHelper($aclHelper);
    }

    public function initDatabase()
    {
        // get config from config/environment.ini
        $config = $this->_config;
        $dbAdapter = Zend_Db::factory($config->database);

        //set default adapter
        Zend_Db_Table::setDefaultAdapter($dbAdapter);

        //save Db in registry for later use
        //when we need it to do a direct query
        Zend_Registry::set("dbAdapter", $dbAdapter);

    }

    public function initRoutes()
    {
        $router = $this->_front->getRouter();
        $router->removeDefaultRoutes();

        $router->addRoutes(array(
                "public" => new Zend_Controller_Router_Route(
                        '/:controller/:action',
                        array('module' => 'public', 'controller' => 'index', 'action' => 'index')),

                "gestionar" => new Zend_Controller_Router_Route(
                        'gestionar/:page',
                        array('module' => 'public', 'controller' => 'static', 'action' => 'index')),

                "login" => new Zend_Controller_Router_Route(
                        'login/:controller/:action',
                        array('module' => 'admin', 'controller' => 'auth', 'action' => 'index')),

                "dashboard" => new Zend_Controller_Router_Route(
                         'admin/:controller/:action',
                         array('module' => 'admin', 'controller' => 'dashboard', 'action' => 'index'))
        ));

    }

}
