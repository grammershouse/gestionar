<?php

/**************************************************
 * Setting the include_path will not include every file in that directory, 
 * it only adds that directory to the list PHP will search when including a file.
 * 
 * Specifies a list of directories where the require(), include(), fopen(), 
 * file(), readfile() and file_get_contents() functions look for files.
 *************************************************/

//dirname(__FILE__), real project path

set_include_path(
    dirname(__FILE__) . PATH_SEPARATOR . 
    dirname(__FILE__) . '/../../library' .  PATH_SEPARATOR . 
    dirname(__FILE__) . '/../../app/models' .  PATH_SEPARATOR .
    get_include_path());
