<?php

abstract class Grammers_Controller extends Zend_Controller_Action
{
    protected function setLayout($layout = 'public'){
        Zend_Layout::startMvc(array(
            'layoutPath' => ROOT . '/app/layouts/scripts',
            'layout' => $layout) //file name
        );
    }

    protected function disableLayout(){
        $this->_helper->viewRenderer->setNoRender(true);
    }

    public function initJsModules(array $jsFiles, $module = 'public'){
        foreach ($jsFiles as $key => $jsf) {
            $jsFiles[$key] = $module.'.'.$jsf;
        }
        $concatenated = "$(document).ready(function(){ console.info(jGrammers); jGrammers.init(" . json_encode($jsFiles) . ") });";
        $this->setJsModules($concatenated, 'header');
    }

    public function setJsModules($concatenated, $place){
        $place = (!is_string($place)) ? $place : 'header';

        switch ($place) {
            case 'header':
                $module['header'] = $concatenated;
                break;

            default:
                $module['bottom'] = $concatenated;
                break;
        }

        $this->view->jsModules = $module;
    }

    public function xhractionAction(){
        /*$this->_helper->viewRenderer->setNoRender(true);*/

        var_dump($_POST);

        exit;

    }
}