<?php
/**
/**Grammers access all constants and methods to deal with acl and auth
**/

class Grammers_Access {

    const SUPER_ADMIN  = 32;
    const ADMIN        = 16;
    const LOGGED_USER  = 8;
    const NO_USER      = 4;

}