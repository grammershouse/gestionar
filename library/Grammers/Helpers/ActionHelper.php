<?php
require_once 'Zend/Controller/Action/Helper/Abstract.php';

class Grammers_Helpers_ActionHelper extends Zend_Controller_Action_Helper_Abstract
{
    protected $_action;
    protected $_controller;
    protected $_module;

    public function init()
    {
            $this->_action = $this->getActionController();
            $this->_controller = $this->_action->getRequest()->getControllerName();
            $this->_module = $this->_action->getRequest()->getModuleName();
    }

    public function preDispatch()
    {
        $request = $this->_action->getRequest();
        $controller = $request->getControllerName();
        $action = $request->getActionName();
        $module = $request->getModuleName();
        $redirectorHelper = $this->_action->getHelper("Redirector");

        $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer');
        $view = $viewRenderer->view;
        $view->isLoggued = $isLoggued = (Grammers_Auth::getInstance()->getIdentity() != null);
        $view->controllerName = $controller;
        $view->actionName = $action;
        $view->moduleName = $module;

        $user = null;
        if ($isLoggued) {
            $view->user = $user = Grammers_Auth::getInstance()->getIdentity();
            $authns = new Zend_Session_Namespace(Grammers_Auth::getInstance()->getStorage()->getNamespace());
            $authns->setExpirationSeconds(60*60); //1 hora
            //echo "init session => ";
        }
        if ( $isLoggued && $module === "admin" &&
            (int)$user->acl === Grammers_Access::SUPER_ADMIN ) {
            // $redirectorHelper->goToUrl(URL_APP . 'login/');
        }else if($isLoggued){
            echo "no rights to see this page !!";
        }
    }
}