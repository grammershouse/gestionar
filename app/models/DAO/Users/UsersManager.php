<?php

class DAO_Users_UsersManager extends Grammers_Db_AbstractManager
{
    protected static $_instance;

    public static function getInstance(){
        if(null == self::$_instance){
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    
    public function __construct()
    {
        parent::__construct();
    }
    
    protected function _getStorage() {
        is_null($this->_storage) && $this->_storage = new DAO_Users_UsersStorage();
        return $this->_storage;
    }     
    
    public function getInitialVo(){
        $vo = new DAO_Users_UsersVo();
        return $vo;        
    }
    
    public function getById($id){
       return $this->_storage->getById($id);
    }
}