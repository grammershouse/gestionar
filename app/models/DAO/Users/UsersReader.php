<?php
class DAO_Users_UsersReader extends Grammers_Db_AbstractReader
{
    protected $_name = "users"; 
    protected $_primary = "id";
    protected $_sequence = true;
    protected static $_instance;
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function getById($id){
        $_sql = <<<EOQ
SELECT
    id,
    acl,
    name,
    lastname,
    document_id,
    cmun_id,
    email,
    address,
    phone,
    cell_phone1,
    cell_phone2,
    created_at,
    updated_at
FROM
    {$this->_name}
WHERE 
    deleted = 0
    and id = {$id}
EOQ;
        return $this->_db->fetchRow($_sql);        
        
    }
    
    public function getTableName(){
        return $this->_name;
    }
    
}