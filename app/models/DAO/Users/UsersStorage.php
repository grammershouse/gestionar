<?php

class DAO_Users_UsersStorage extends Grammers_Db_AbstractStorage
{
    protected $_reader = null;
    protected $_writer = null;
    
    protected function _initReaderAndWriter() {
        $this->_reader = new DAO_Users_UsersReader();
        $this->_writer = new DAO_Users_UsersWriter();
    }
    
    public function getById($id){
       return $this->_reader->getById($id);
    }
    
}