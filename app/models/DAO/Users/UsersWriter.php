<?php
class DAO_Users_UsersWriter extends Grammers_Db_AbstractWriter
{
    protected $_name = "users"; 

    protected static $_instance;
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function getTableName(){
        return $this->_name;
    }    
    
    public function insert($object){
        
        $sql = <<<EOL
        INSERT INTO
            {$this->_name}
            (
            acl,
            name,
            lastname,
            document_id,
            cmun_id,
            email,
            address,
            phone,
            cell_phone1,
            cell_phone2,
            created_at,
            updated_at,
            salt,
            password,
            deleted
            )
        VALUES
            (?, ?, ?, ?, ?, ?)
EOL;

        $this->_db->query($sql, array(
            $object->acl,
            $object->name,
            $object->lastname,
            $object->document_id,
            $object->cmun_id,
            $object->email,
            $object->address,
            $object->phone,
            $object->cell_phone1,
            $object->cell_phone2,
            $object->created_at,
            $object->updated_at,
            md5($object->email),
            $this->setPassword($object),
            $object->deleted
        ));

        $id = $this->_db->lastInsertId($this->getTableName, "id");
        if ($id > 0) {
            $object->id = $id;
            return true;
        }
        else {
            return false;
        }        
        
    }
    
    public function update($object){
        
        $sql = <<<EOL
        UPDATE
            {$this->_name}
        SET
            name = ?,
            lastname = ?,
            email = ?,
            nick = ?,
            password = ?, //warning to do salt
            acl = ? 
        WHERE
            id = ?
EOL;


    	try {
                $this->_db->query($sql, array(
                    $object->name,
                    $object->lastname,
                    $object->email,
                    $object->nick,
                    $object->password,
                    $object->acl

                ));
    	} catch (Exception $e) {
                echo $e->getMessage();
    	    return false;
    	}

    	return true;        
        
    }
    
    public function deleteById($object){
        
    }

    private function setPassword($vo){
        return md5($vo->email) . ':' . md5($vo->password);
    }
}