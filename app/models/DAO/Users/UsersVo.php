<?php
//maybe in the future i will need to create an 
//abstract class to deal with a bigger Vo definition, 
//for the moment it's simple

class DAO_Users_UsersVo
{
    public $id = 0;
    public $acl = 0;
    public $name = '';
    public $lastname = '';
    public $document_id = 0;
    public $cmund_id = 0;
    public $email = '';
    public $address = '';
    public $phone = '';
    public $cell_phone1 = '';
    public $cell_phone2 = '';
    public $create_at = '';
    public $updated_at = 0;
    public $password = '';
    public $deleted = 0;
}
