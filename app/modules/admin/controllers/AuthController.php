<?php

class Admin_AuthController extends Grammers_Controller
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
         $this->setLayout('public');
    }

    public function loginAction(){
        $this->disableLayout();
        $usersManager = DAO_Users_UsersManager::getInstance();

        $email = $this->_request->getParam('email',null);
        $password = $this->_request->getParam('password',null);

        $grammersAuth = Grammers_Auth::getInstance();
        $authentication = $grammersAuth->login($email, $password);

        if($authentication['status'] == 'ok'){
            $user = $grammersAuth->getIdentity();
            if((int) $user->acl === Grammers_Access::SUPER_ADMIN){
                $this->_redirect('admin/dashboard');
            }
        }else{
            print_r('not authenticated');
           // $this->_redirect('/login');
        }
    }

    public function logoutAction(){
        // clear everything - session is cleared also!
        Zend_Auth::getInstance()->clearIdentity();
        $this->_redirect('/login/');
    }


}

