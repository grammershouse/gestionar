<?php

class Admin_DashboardController extends Grammers_Controller
{

    public function init()
    {
        $this->setLayout('admin');
    }

    public function indexAction()
    {
         $user = Grammers_Auth::getInstance()->getIdentity();
         if($user){
			$this->view->user = $user;
         }else{
         	//$this->_redirect(URL_APP, array());
         }
    }

    /**
    ** static pages action
    **/
    public function spagesAction()
    {
        $jsModules = array('staticPages');
        $this->initJsModules($jsModules, 'admin');
    }

}