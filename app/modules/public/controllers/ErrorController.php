<?php

class ErrorController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $errorHandler = $this->getRequest()->getParam('error_handler');
        $error = $errorHandler->exception;
        echo $error->getMessage();
    }    


}

