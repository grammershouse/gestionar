if(typeof(jGrammers) == 'undefined'){ jGrammers = {} }

jGrammers = new function(config){

	this.modules = [];
	this.URL_APP = window.location.origin;

	this.init = function(config){
		if(typeof(config) == 'object'){
			for(var i in config){
				if(typeof(this.modules[config[i]]) == 'undefined'){
					var tmpName = "jGrammers." + config[i];
					var tmpClass = new this.toFunction(tmpName);
					this.modules[config[i]] = new tmpClass();
					this.modules[config[i]].init();
				}
			}
		}
	};

	this.getModules = function(){
		return true;
	};

	this.toFunction = function(str) {
	  if (typeof str !== 'string') {
	      return false;
	  }

	  var arr = str.split(".");

	  var fn = window;
	  for (var i = 0, len = arr.length; i < len; i++) {
	    fn = fn[arr[i]];
	  }

	  if (typeof fn !== "function") {
	    console.info('error !!!');
	  }

	  return  fn;
	};


}