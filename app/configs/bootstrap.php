<?php

/**************************************************
 * 
 * bootstrap.php
 * ------------------
 * [ application requirement !IMPORTANT ]
 *
 *************************************************/


require_once '../library/Grammers/Includes.php';
// AutoLoader not started yet
require_once '../library/Grammers/Initializer.php';

// Prepare the front controller. 
$frontController = Zend_Controller_Front::getInstance();

// Change to 'production' parameter under production environment
$frontController->registerPlugin(new Grammers_Initializer());

// Dispatch the request using the front controller. 
$frontController->dispatch();