var gulp      = require('gulp'),
    uglify    = require('gulp-uglify'),
    concat    = require('gulp-concat'),
    minifyCSS = require('gulp-minify-css'),
    watch     = require('gulp-watch');


gulp.task('concatCssAdmin', function(){
        gulp.src(['app/modules/admin/views/css/dashboard.css'])
        .pipe(minifyCSS({ keepSpecialComments: '*', keepBreaks: '*' }))
        .pipe(concat('adminCss.css'))
        .pipe(gulp.dest('public/css'))
});

gulp.task('concatJsAdmin', function(){
        gulp.src(['public/components/js/jquery-1.11.0.min.js',
                  'public/components/js/hideshow.js',
                  'public/components/js/jquery.tablesorter.min.js',
                  'public/components/js/jquery.equalHeight.js',
                  'public/components/js/atomic.js',
                  'app/commonjs/Common.js',
                  'app/modules/admin/views/js/StaticPages.js',
                 ])
        /*.pipe(uglify({ compress: true }))*/
        .pipe(concat('adminJs.js'))
        .pipe(gulp.dest('public/js'))
});

gulp.task('concatJsPublic', function(){
    gulp.src(['public/components/js/jquery-1.11.0.min.js',
              'public/components/js/modernizr.js',
              'public/components/js/foundation.min.js'
             ])
    .pipe(uglify({ compress: true }))
    .pipe(concat('publicJs.js'))
    .pipe(gulp.dest('public/js'))
});

gulp.task('concatSliderJsHome', function(){
    gulp.src(['public/components/js/jssor.core.js',
              'public/components/js/jssor.utils.js',
              'public/components/js/jssor.slider.js',
              'public/components/js/slide.js'
             ])
    .pipe(uglify({ compress: true }))
    .pipe(concat('sliderJs.js'))
    .pipe(gulp.dest('public/js'))
});

gulp.task('concatCssPublic', function(){
        gulp.src(['public/components/css/foundation-icons.css',
                  'app/modules/public/views/css/public.css',
                  'app/modules/public/views/css/footer.css',
                  'public/components/css/foundation.min.css',
                  'public/components/css/slider.css',
                  'public/components/css/normalize.css'
                 ])
        .pipe(minifyCSS({ keepSpecialComments: '*', keepBreaks: '*' }))
        .pipe(concat('publicCss.css'))
        .pipe(gulp.dest('public/css'))
});

gulp.task('default', ['concatJsPublic', 'concatCssPublic','concatJsAdmin','concatCssAdmin', 'concatSliderJsHome']);
